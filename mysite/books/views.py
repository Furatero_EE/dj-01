from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from books.models import Book, Publisher, Author
from .forms import ContactForm

# Create your views here.

#def search_form(request):
	#return render(request, 'search_form.html')


def search(request):
	errors = []
	if 'q' in request.GET:
		q = request.GET['q']
		if not q:
			errors.append('Enter a search term.')
		elif len(q) > 20:
			errors.append('Please enter at most 20 characters.')
		else:
			books = Book.objects.filter(title__icontains=q)
			return render(
				request,
				'search_results.html',
				{'books': books, 'query': q},
			)
	return render(request, 'search_form.html', {'errors': errors})


def search_publisher(request):
	errors = []
	if 'q' in request.GET:
		q = request.GET['q']
		if not q:
			errors.append('Enter a search term.')
		elif len(q) > 20:
			errors.append('Please enter at most 20 characters.')
		else:
			publishers = Publisher.objects.filter(name__icontains=q)
			return render(
				request,
				'search_publisher_results.html',
				{'publishers': publishers, 'query': q}
			)
	return render(request, 'search_form.html', {'errors': errors})


def search_author(request):
	errors = []
	if 'q' in request.GET:
		q = request.GET['q']
		if not q:
			errors.append('Enter a search term.')
		elif len(q) > 20:
			errors.append('Please enter at most 20 characters.')
		else:
			authors = Author.objects.filter(first_name__icontains=q)
			return render(
				request,
				'search_author_results.html',
				{'authors': authors, 'query': q}
			)
	return render(request, 'search_form.html', {'errors': errors})


def contact(request):
	if request.method == 'POST': # If the form has been submitted...
		form = ContactForm(request.POST) # A form bound to the POST data
		if form.is_valid(): # All validation rules pass
			# Process the data in form.cleaned_data
			# ...
			return HttpResponseRedirect('/thanks/')
		else:
			form = ContactForm() # An unbound form
		return render(
			request,
			'contact.html',
			{'form': form},
		)